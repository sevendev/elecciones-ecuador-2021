library(tidyverse)
source("R/helpers.R")

# Enlace a la API del CNE
URL <- 'https://resultados2021.cne.gob.ec/Home/ConsultarResultados'

#Datos a nivel nacional para presidencia
resp <- POST(url = URL, body = build_body(7, 17), encode = 'form')
data <- content(resp, type = "text") %>%
  jsonlite::fromJSON()

#Gráfico 1: Porcentaje de Mujeres vs Hombres con respeto al total de votantes
data$datos %>% 
  arrange(intVotos) %>%
  mutate(intVotosH = -100 * intVotosH / sum(intVotos),
         intVotosM = 100 * intVotosM / sum(intVotos),
         strNomCandidato = str_to_title(strNomCandidato),
         intVotos = intVotos / sum(intVotos)) %>%
  select(Candidato = strNomCandidato, Total = intVotos, 
         Mujeres = intVotosM, Hombres = intVotosH) %>%
  pivot_longer(cols = Mujeres:Hombres, names_to = 'Género') %>%
  ggplot(aes(forcats::fct_reorder(Candidato, Total), 
             value, fill = `Género`)) +
  geom_col() +
  scale_y_continuous(breaks = seq(-100, 100, by = 5), 
                     labels = paste0(abs(seq(-100, 100, by = 5)), " %")) +
  coord_flip() +
  labs(title = "Porcentaje de Mujeres vs Hombres con respeto al total de votantes",
       subtitle = glue::glue("Fecha de corte: {data$fechaCorte} GMT"),
       x = "", y = '') +
  theme_bw()

#Gráfico 2: Porcentaje de Mujeres vs Hombres con respeto a la votación del 
#           candidato
data$datos %>% 
  mutate(intVotosH = -100 * intVotosH / intVotos,
         intVotosM = 100 * intVotosM / intVotos,
         strNomCandidato = str_to_title(strNomCandidato)) %>%
  select(Candidato = strNomCandidato, Mujeres = intVotosM, Hombres = intVotosH) %>%
  pivot_longer(cols = -Candidato, names_to = 'Género') %>%
  ggplot(aes(Candidato, value, fill = `Género`)) +
  geom_col() +
  scale_y_continuous(breaks = seq(-100, 100, by = 10), 
                     labels = paste0(abs(seq(-100, 100, by = 10)), " %")) +
  coord_flip() +
  labs(title = "Porcentaje de Mujeres vs Hombres con respeto \na la votación del candidato",
       subtitle = glue::glue("Fecha de corte: {data$fechaCorte} GMT"),
       x = "", y = '') +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = .5))
